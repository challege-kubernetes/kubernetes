#!/usr/bin/bash

sh mongo/run.sh
sh ratings/run.sh
sh details/run.sh
sh reviews/run.sh
sh product-page/run.sh
minikube kubectl -- apply -f ingress.yml