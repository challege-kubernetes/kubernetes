# kubernetes

This project deploys a demo application on a kubernetes (in this case minikube) cluster. There is a script to ease the deployment.

## how to run it

1. start minikube `minikube start`
2. enable ingress `minikube addons enable ingress`
3. run script `./run.sh`
4. browse to theurl specified in the ingress.yml file


If anything goes wrong you can still create each component manually with `kubectl apply -f ` etc. 

Remember **the order is important!**, namespaces should be created first