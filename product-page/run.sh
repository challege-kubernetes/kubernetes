#!/usr/bin/bash

cd $(dirname $0)
minikube kubectl -- apply -f product-page-ns.yml -f product-page-cm.yml -f product-page.yml -f product-page-service.yml