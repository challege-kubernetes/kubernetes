#!/usr/bin/bash

cd $(dirname $0)
minikube kubectl -- apply -f ./mongo-ns.yml -f ./mongo-pv.yml -f ./mongo-pvc.yml -f ./mongo.yml -f ./mongo-service.yml