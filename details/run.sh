#!/usr/bin/bash

cd $(dirname $0)
minikube kubectl -- apply -f details-ns.yml -f details-cm.yml -f details.yml -f details-service.yml