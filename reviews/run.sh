#!/usr/bin/bash

cd $(dirname $0)
minikube kubectl -- apply -f reviews-ns.yml -f reviews-cm.yml -f reviews.yml -f reviews-service.yml