#!/usr/bin/bash

cd $(dirname $0)
minikube kubectl -- apply -f ratings-ns.yml -f ratings-cm.yml -f ratings.yml -f ratings-service.yml